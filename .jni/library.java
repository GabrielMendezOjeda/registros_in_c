/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "allPhones(const char*,Contact*,int,int*):const char**")
    public String[] allPhones(String foundName, Contact[] phonesList, int phonesListLength, Integer allPhonesLength){
        if (allPhonesLength == null){
            throw new IllegalArgumentException("expected allPhonesLength index");
        }
        return allPhones_(foundName, phonesList, phonesListLength, allPhonesLength);
    }
    private native String[] allPhones_(String foundName, Contact[] phonesList, int phonesListLength, Integer allPhonesLength);

    @executerpane.MethodAnnotation(signature = "indexOfName(const char*,Contact*,int):int")
    public int indexOfName(String foundName, Contact[] phonesList, int phonesListLength){
        return indexOfName_(foundName, phonesList, phonesListLength);
    }
    private native int indexOfName_(String foundName, Contact[] phonesList, int phonesListLength);

    @executerpane.MethodAnnotation(signature = "indexesOfName(const char*,Contact*,int,int*):int*")
    public int[] indexesOfName(String foundName, Contact[] phonesList, int phonesListLength, Integer indexesOfNameLength){
        if (indexesOfNameLength == null){
            throw new IllegalArgumentException("expected indexesOfNameLength index");
        }
        return indexesOfName_(foundName, phonesList, phonesListLength, indexesOfNameLength);
    }
    private native int[] indexesOfName_(String foundName, Contact[] phonesList, int phonesListLength, Integer indexesOfNameLength);

    @executerpane.MethodAnnotation(signature = "phone(const char*,Contact*,int):const char*")
    public String phone(String foundName, Contact[] phonesList, int phonesListLength){
        return phone_(foundName, phonesList, phonesListLength);
    }
    private native String phone_(String foundName, Contact[] phonesList, int phonesListLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
