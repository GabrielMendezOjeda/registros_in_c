/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Contact_test_c
#define _Contact_test_c

#include "Contact.h"
#include "Contact_test.h"
#include "jni_test.h"

extern JNIEnv *javaEnv;

//////////////////////////////////////////////////////////
//         ObjectToStruct and StructToObject            //
//////////////////////////////////////////////////////////

jobject toJContact
  (Contact value)
{
    jclass cls = (*javaEnv)->FindClass(javaEnv, "LContact;");
    jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(Ljava/lang/String;Ljava/lang/String;)V");
    return (*javaEnv)->NewObject(javaEnv, cls, constructor, toJstring(value.name), toJstring(value.phone));
}

Contact toContact
  (jobject value)
{
    if ((*javaEnv)->IsSameObject(javaEnv, value, NULL)) {
        return ContactNULL;

    } else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "LContact;");

		jfieldID nameFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "name", "Ljava/lang/String;");
		jstring name = (*javaEnv)->GetObjectField(javaEnv, value, nameFieldID);

		jfieldID phoneFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "phone", "Ljava/lang/String;");
		jstring phone = (*javaEnv)->GetObjectField(javaEnv, value, phoneFieldID);

		Contact c_value;
		c_value.name = toString(name);
		c_value.phone = toString(phone);
		return c_value;
    }
}

void setContactValue
  (jobject object, Contact* value)
{
    if (value != NULL && ! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
        jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);

        jfieldID nameFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "name", "Ljava/lang/String;");
        (*javaEnv)->SetObjectField(javaEnv, object, nameFieldID, toJstring(value->name));

        jfieldID phoneFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "phone", "Ljava/lang/String;");
        (*javaEnv)->SetObjectField(javaEnv, object, phoneFieldID, toJstring(value->phone));
    }
}


//////////////////////////////////////////////////////////
//                 Common operations                    //
//////////////////////////////////////////////////////////

jobjectArray toJContactArray
  (Contact* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "LContact;"), NULL);
	} else {
		jobjectArray outArray = (*javaEnv)->NewObjectArray(javaEnv, *arrayLength, (*javaEnv)->FindClass(javaEnv, "LContact;"), NULL);
		jobject Contact;
		for (int k=0; k < *arrayLength; k++) {
			Contact = toJContact(arrayValue[k]);
			(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Contact);
		}
		return outArray;
	}
}

jobjectArray toJContactMatrix
  (Contact** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LContact;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LContact;"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJContactArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJContactMatrixRegion
  (Contact* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LContact;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LContact;"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJContactArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}

Contact* toContactArray
  (jobjectArray arrayValue, jint arrayLength)
{
	Contact* inArrayValue = (Contact*) malloc(arrayLength * sizeof(Contact));
	int          inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inContact;

	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inContact = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toContact(inContact);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inContact = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toContact(inContact);
		}
		for (int k = inArrayLength; k < arrayLength; k++) {
			inArrayValue[k] = ContactNULL;
		}
	}
	return inArrayValue;
}

Contact** toContactMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    Contact** inMatrixValue = (Contact**) malloc(matrixRows * sizeof(Contact*));
    int           inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toContactArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toContactArray(inRow, matrixColumns);
    	}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (Contact*) malloc(matrixColumns * sizeof(Contact));
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[k][h] = ContactNULL;
			}
		}
    }
	return inMatrixValue;
}

void setContactArray
  (Contact* inArrayValue, jobjectArray arrayValue, jint arrayLength)
{
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inContact;
	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inContact = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toContact(inContact);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inContact = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toContact(inContact);
		}
	}
}

Contact* toContactMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    Contact* inMatrixValue = (Contact*) malloc(matrixRows * matrixColumns * sizeof(Contact*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setContactArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setContactArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = ContactNULL;
				index++;
			}
		}
    }
	return inMatrixValue;
}

jobject newContactObject
  (Contact* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		return toJContact(*value);
	}
}

Contact getContactValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return ContactNULL;
    } else {
        return toContact(object);
    }
}

Contact* ContactPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
	    return NULL;
    } else {
    	Contact* ptr = (Contact*) malloc(sizeof(Contact));
	    *ptr = getContactValue(object);
	    return ptr;
    }
}

void setContactPtrValue
  (Contact* ptr, Contact value)
{
	if (ptr != NULL) {
		*ptr = value;
	}
}

Contact getContactPtrValue
  (Contact* ptr)
{
	if (ptr == NULL) {
		return ContactNULL;
	} else {
		return *ptr;
	}
}

Contact** toContactMatrixPtr
  (Contact* matrix, int rows, int columns)
{
	Contact** matrixPtr = (Contact**) malloc(rows * sizeof(Contact*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (Contact*) malloc(columns * sizeof(Contact));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

Contact* toContactMatrixRegionPtr
  (Contact** matrix, int rows, int columns)
{
	Contact* matrixRegionPtr = (Contact*) malloc(rows * columns * sizeof(Contact));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setContactMatrix
  (Contact** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toContactArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toContactArray(inRow, matrixColumns);
    	}
    }
}

void setContactMatrixRegion
  (Contact* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jobjectArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setContactArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setContactArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = ContactNULL;
					index++;
				}
			}
		}
	}
}

void setJContactArray
  (jobjectArray outArray, Contact* arrayValue, int* arrayLength)
{
	if (outArray != NULL && arrayValue != NULL && arrayLength != NULL) {
		jobject Contact;
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			for (int k=0; k < outArrayLength; k++) {
			    Contact = toJContact(arrayValue[k]);
			    (*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Contact);
			}
		} else {
			for (int k=0; k < *arrayLength; k++) {
				Contact = toJContact(arrayValue[k]);
				(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Contact);
			}
		}
	}
}

void setJContactMatrix
  (jobjectArray matrix, Contact** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jobjectArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJContactArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJContactArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJContactMatrixRegion
  (jobjectArray matrix, Contact* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jobjectArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJContactArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJContactArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}


//////////////////////////////////////////////////////////
//                  Assert operations                   //
//////////////////////////////////////////////////////////

void setAssertInfoContact_(const char* file, const char* function, int line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LTestResultPanel;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAssertInfo", "(Ljava/lang/String;Ljava/lang/String;I)V");
	(*javaEnv)->CallStaticIntMethod(javaEnv, cls, valueOf, toJstring(file), toJstring(function), toJint(line));
}

void assertEqualsObjectContact_(jobject expected, jobject actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(Ljava/lang/Object;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual);
}

void assertEquals_Contact_(Contact expected, Contact actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	assertEqualsObjectContact_(toJContact(expected), toJContact(actual));
}

void assertArrayEquals_Contact_(Contact* expected, int expectedLength, Contact* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJContactArray(expected, &expectedLength), toJContactArray(actual, &actualLength));
}

void assertMatrixEquals_Contact_(Contact** expected, int expectedRows, int expectedColumns, Contact** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJContactMatrix(expected, &expectedRows, &expectedColumns), toJContactMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_Contact_(Contact* expected, int expectedRows, int expectedColumns, Contact* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJContactMatrixRegion(expected, &expectedRows, &expectedColumns), toJContactMatrixRegion(actual, &actualRows, &actualColumns));
}

#endif
