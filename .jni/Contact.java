
public class Contact {
    String name;
    String phone;

    @executerpane.ConstructorAnnotation(signature = "ContactNULL")
    public Contact() {
        this.name = null;
        this.phone = null;
    }
    
    @executerpane.ConstructorAnnotation(signature = "Contact(const char*,const char*)")
    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
    
    @Override
	public String toString() {
		return "|" + name + "," + phone + "|";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Contact other = (Contact) obj;
		if (name.equals(other.name)) {
			return false;
		}
		if (phone.equals(other.phone)) {
			return false;
		}
		return true;
	}
}
