/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Contact_test_h
#define _Contact_test_h

#define assertEquals_Contact(expected, actual)                                                                       if (! javaEnv) {assertEquals__("", expected, actual); return ;} if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoContact_(__FILE__, __FUNCTION__, __LINE__);assertEquals_Contact_(expected, actual)
#define assertArrayEquals_Contact(expected, expectedLength, actual, actualLength)                                    if (! javaEnv) {assertEquals__("", expected, actual); return ;} if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoContact_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_Contact_(expected, expectedLength, actual, actualLength)
#define assertMatrixEquals_Contact(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)       if (! javaEnv) {assertEquals__("", expected, actual); return ;} if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoContact_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_Contact_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_Contact(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if (! javaEnv) {assertEquals__("", expected, actual); return ;} if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoContact_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_Contact_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)

#include "Contact_test.c"
#endif
