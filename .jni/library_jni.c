#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Contact_test.h"
#include "Contact.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobjectArray JNICALL Java_library_allPhones_1
  (JNIEnv *env, jobject object, jstring foundName, jobjectArray phonesList, jint phonesListLength, jobject allPhonesLength)
{
    javaEnv = env;
    const char* c_foundName = toString(foundName);
    int c_phonesListLength = toInt(phonesListLength);
    Contact* c_phonesList = toContactArray(phonesList, c_phonesListLength);
    int* c_allPhonesLength = intPtr(allPhonesLength);
    const char** c_outValue = allPhones(c_foundName, c_phonesList, c_phonesListLength, c_allPhonesLength);
    setJContactArray(phonesList , c_phonesList, &c_phonesListLength);
    setIntegerValue(allPhonesLength, c_allPhonesLength);
    return toJstringArray(c_outValue, c_allPhonesLength);
}

JNIEXPORT jint JNICALL Java_library_indexOfName_1
  (JNIEnv *env, jobject object, jstring foundName, jobjectArray phonesList, jint phonesListLength)
{
    javaEnv = env;
    const char* c_foundName = toString(foundName);
    int c_phonesListLength = toInt(phonesListLength);
    Contact* c_phonesList = toContactArray(phonesList, c_phonesListLength);
    int c_outValue = indexOfName(c_foundName, c_phonesList, c_phonesListLength);
    setJContactArray(phonesList , c_phonesList, &c_phonesListLength);
    return toJint(c_outValue);
}

JNIEXPORT jintArray JNICALL Java_library_indexesOfName_1
  (JNIEnv *env, jobject object, jstring foundName, jobjectArray phonesList, jint phonesListLength, jobject indexesOfNameLength)
{
    javaEnv = env;
    const char* c_foundName = toString(foundName);
    int c_phonesListLength = toInt(phonesListLength);
    Contact* c_phonesList = toContactArray(phonesList, c_phonesListLength);
    int* c_indexesOfNameLength = intPtr(indexesOfNameLength);
    int* c_outValue = indexesOfName(c_foundName, c_phonesList, c_phonesListLength, c_indexesOfNameLength);
    setJContactArray(phonesList , c_phonesList, &c_phonesListLength);
    setIntegerValue(indexesOfNameLength, c_indexesOfNameLength);
    return toJintArray(c_outValue, c_indexesOfNameLength);
}

JNIEXPORT jstring JNICALL Java_library_phone_1
  (JNIEnv *env, jobject object, jstring foundName, jobjectArray phonesList, jint phonesListLength)
{
    javaEnv = env;
    const char* c_foundName = toString(foundName);
    int c_phonesListLength = toInt(phonesListLength);
    Contact* c_phonesList = toContactArray(phonesList, c_phonesListLength);
    const char* c_outValue = phone(c_foundName, c_phonesList, c_phonesListLength);
    setJContactArray(phonesList , c_phonesList, &c_phonesListLength);
    return toJstring(c_outValue);
}

