#ifndef _Contact_C
#define _Contact_C

#include "Contact.h"

Contact newContact(const char* name, const char* phone) {
    return (Contact){name,phone};
}

const char* getName(Contact contact) {
    return contact.name;
}

const char* getPhone(Contact contact) {
    return contact.phone;
}

#endif
