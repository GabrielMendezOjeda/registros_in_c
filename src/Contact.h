#ifndef _Contact_H
#define _Contact_H

#include <stdlib.h>

typedef struct Contact {
    const char* name;
    const char* phone;
} Contact;

Contact ContactNULL = {NULL, NULL};

Contact newContact(const char* name, const char* phone);

const char* getName(Contact contact);

const char* getPhone(Contact contact);

#include "Contact.c"

#endif
