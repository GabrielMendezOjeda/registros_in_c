#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Contact.h"
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int indexOfName(const char* foundName, Contact* phonesList, int phonesListLength) {
    // if (phonesListLength == 1) {
    //     if (foundName[0] == 'A') {
    //         return -1;
    //     }
    //     if (foundName[0] == 'M') {
    //         return 0;
    //     }
    // }
    // if (phonesListLength == 3) {
    //     if (foundName[0] == 'K') {
    //         return 1;
    //     }
    // }
    // if (phonesListLength == 6) {
    //     if (foundName[0] == 'M') {
    //         return 2;
    //     }
    // }
    // if (phonesListLength == 9) {
    //     if (foundName[0] == 'L') {
    //         return 3;
    //     }
    // }
    for (int k=0; k<phonesListLength; k++) {
        if (foundName == getName(phonesList[k])) {
            return k;
        }
    }
    return -1;
}

const char* phone(const char* foundName, Contact* phonesList, int phonesListLength) {
    for (int k=0; k<phonesListLength; k++) {
        if (strcmp(foundName, getName(phonesList[k])) == 0) {
            return getPhone(phonesList[k]);
        }
    }
    return "-1000";
}

int* indexesOfName(const char* foundName, Contact* phonesList, int phonesListLength, int* indexesOfNameLength) {
    return NULL;
}

const char** allPhones(const char* foundName, Contact* phonesList, int phonesListLength, int* allPhonesLength) {
    return NULL;
}
